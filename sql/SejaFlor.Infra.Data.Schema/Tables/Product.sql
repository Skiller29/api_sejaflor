﻿CREATE TABLE [dbo].[Product]
(
	[ProductId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(100) NOT NULL, 
    [CostAmount] DECIMAL NOT NULL, 
    [SaleAmountInCash] DECIMAL NOT NULL, 
    [SaleAmountInCard] DECIMAL NOT NULL, 
    [MainPictureUrl] VARCHAR(250) NULL,

	[CreationTime] DATETIME NOT NULL, 
    [CreativeUserId] UNIQUEIDENTIFIER NOT NULL, 
    [LastModificationTime] DATETIME NULL, 
    [LastModifierUserId] UNIQUEIDENTIFIER NULL, 
    [DeletionTime] DATETIME NULL, 
    [DeletionUserId] UNIQUEIDENTIFIER NULL, 
    [IsDeleted] BIT NOT NULL DEFAULT 0,

	CONSTRAINT [UK_Product] UNIQUE ([Name], [IsDeleted], [DeletionTime]),
	CONSTRAINT [PK_Product] PRIMARY KEY ([ProductId]),
	CONSTRAINT FK_ProductCreativeUser FOREIGN KEY (CreativeUserId) REFERENCES [User](UserId),
	CONSTRAINT FK_ProductLastModifierUser FOREIGN KEY (LastModifierUserId) REFERENCES [User](UserId),
	CONSTRAINT FK_ProductDeletionUser FOREIGN KEY (DeletionUserId) REFERENCES [User](UserId)
)
