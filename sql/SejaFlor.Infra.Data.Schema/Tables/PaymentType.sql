﻿CREATE TABLE [dbo].[PaymentType]
(
	[PaymentTypeId] INT NOT NULL, 
    [Name] VARCHAR(80) NOT NULL,

	CONSTRAINT [PK_PaymentType] PRIMARY KEY ([PaymentTypeId]),

)
