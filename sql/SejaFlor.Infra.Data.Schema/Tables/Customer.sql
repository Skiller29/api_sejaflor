﻿CREATE TABLE [dbo].[Customer]
(
	[CustomerId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(100) NOT NULL, 
    [Phone] VARCHAR(11) NULL, 
    [Address] VARCHAR(150) NULL, 
    [Neighborhood] VARCHAR(60) NULL, 
    [Genre] INT NOT NULL, 
    [Reference] VARCHAR(100) NULL, 
    
	[CreationTime] DATETIME NOT NULL, 
    [CreativeUserId] UNIQUEIDENTIFIER NOT NULL, 
    [LastModificationTime] DATETIME NULL, 
    [LastModifierUserId] UNIQUEIDENTIFIER NULL, 
    [DeletionTime] DATETIME NULL, 
    [DeletionUserId] UNIQUEIDENTIFIER NULL, 
    [IsDeleted] BIT NOT NULL DEFAULT 0,

	CONSTRAINT [PK_Customer] PRIMARY KEY ([CustomerId]),
	CONSTRAINT [UK_Customer] UNIQUE ([Name], [IsDeleted], [DeletionTime]),
	CONSTRAINT FK_CustomerCreativeUser FOREIGN KEY (CreativeUserId) REFERENCES [User](UserId),
	CONSTRAINT FK_CustomerLastModifierUser FOREIGN KEY (LastModifierUserId) REFERENCES [User](UserId),
	CONSTRAINT FK_CustomerDeletionUser FOREIGN KEY (DeletionUserId) REFERENCES [User](UserId)
)