﻿CREATE TABLE [dbo].[Installment]
(
	[InstallmentId] UNIQUEIDENTIFIER NOT NULL, 
    [DueDate] DATETIME NOT NULL, 
    [Amount] DECIMAL NOT NULL, 
    [NumberInstallment] INT NOT NULL, 
    [IsPaid] BIT NOT NULL DEFAULT 0, 
    [PaymentDate] DATETIME NULL, 
    [SaleId] UNIQUEIDENTIFIER NOT NULL,
	[PayerUserId] UNIQUEIDENTIFIER NULL, 

    CONSTRAINT [PK_Installment] PRIMARY KEY ([InstallmentId]),
	CONSTRAINT FK_Installment_Sale FOREIGN KEY (SaleId) REFERENCES Sale(SaleId),
	CONSTRAINT FK_Installment_User FOREIGN KEY (PayerUserId) REFERENCES [User](UserId)
);

GO 

CREATE INDEX 
	x_Installment_SaleId ON Installment( [SaleId] )
