﻿CREATE TABLE [dbo].[Sale]
(
	[SaleId] UNIQUEIDENTIFIER NOT NULL, 
    [SaleDate] DATETIME NOT NULL, 
    [Total] DECIMAL NOT NULL, 
    [TotaCost] DECIMAL NOT NULL, 
    [Installment] BIT NOT NULL DEFAULT 0, 
    [NumberInstallments] INT NULL, 
    [DueDate] DATETIME NULL, 
    [PaymentTypeId] INT NOT NULL, 
    [CustomerId] UNIQUEIDENTIFIER NOT NULL,

	[CreationTime] DATETIME NOT NULL, 
    [CreativeUserId] UNIQUEIDENTIFIER NOT NULL, 
    [DeletionTime] DATETIME NULL, 
    [DeletionUserId] UNIQUEIDENTIFIER NULL, 
    [IsDeleted] BIT NOT NULL DEFAULT 0,

	CONSTRAINT [PK_Sale] PRIMARY KEY ([SaleId]),
	CONSTRAINT FK_Sale_PaymentType FOREIGN KEY (PaymentTypeId) REFERENCES PaymentType(PaymentTypeId),
	CONSTRAINT FK_Sale_Customer FOREIGN KEY (CustomerId) REFERENCES Customer(CustomerId),
	CONSTRAINT FK_SaleCreativeUser FOREIGN KEY (CreativeUserId) REFERENCES [User](UserId),
	CONSTRAINT FK_SaleDeletionUser FOREIGN KEY (DeletionUserId) REFERENCES [User](UserId)
);

GO

CREATE INDEX 
	x_Sale_PaymentTypeId ON Sale( [PaymentTypeId] )
GO

CREATE INDEX 
	x_Sale_CustomerId ON Sale( [CustomerId] )
GO 


