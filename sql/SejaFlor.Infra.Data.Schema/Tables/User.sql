﻿CREATE TABLE [dbo].[User]
(
	[UserId] UNIQUEIDENTIFIER NOT NULL, 
    [UserName] VARCHAR(50) NOT NULL, 
    [Email] VARCHAR(70) NOT NULL, 
    [Password] NVARCHAR(512) NOT NULL, 
    [FirstName] VARCHAR(30) NOT NULL, 
    [LastName] VARCHAR(80) NOT NULL, 
    [CreationTime] DATETIME NOT NULL, 
    [IsDeleted] BIT NOT NULL DEFAULT 0, 
    [DeletionTime] DATETIME NULL,

	CONSTRAINT [PK_User] PRIMARY KEY ([UserId]),
	CONSTRAINT [UK_User_UserName] UNIQUE ([UserName]),
	CONSTRAINT [UK_User_Email] UNIQUE ([Email]),
)
