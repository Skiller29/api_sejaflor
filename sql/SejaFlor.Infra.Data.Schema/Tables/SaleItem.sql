﻿CREATE TABLE [dbo].[SaleItem]
(
	[SaleItemId] UNIQUEIDENTIFIER NOT NULL, 
    [Quantity] INT NOT NULL, 
    [Amount] DECIMAL NOT NULL, 
    [ProductId] UNIQUEIDENTIFIER NOT NULL, 
    [SaleId] UNIQUEIDENTIFIER NOT NULL,

	CONSTRAINT [PK_SaleItem] PRIMARY KEY ([SaleItemId]),
	CONSTRAINT FK_SaleItem_Product FOREIGN KEY (ProductId) REFERENCES Product(ProductId),
	CONSTRAINT FK_SaleItem_Sale FOREIGN KEY (SaleId) REFERENCES Sale(SaleId)
);

GO 

CREATE INDEX 
	x_SaleItem_ProductId ON SaleItem( [ProductId] )
GO

CREATE INDEX 
	x_SaleItem_SaleId ON SaleItem( [SaleId] )
GO
