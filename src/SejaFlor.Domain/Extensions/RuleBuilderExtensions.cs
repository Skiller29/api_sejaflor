﻿using FluentValidation;

namespace SejaFlor.Domain.Extensions
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilder<T, string> IsPassword<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder
                .Matches("[A-Z]").WithMessage("A senha deve conter no mínimo uma letra maiúscula.")
                .Matches("[a-b]").WithMessage("A senha deve conter no mínimo uma letra minúscula.")
                .Matches("[0-9]").WithMessage("A senha deve conter no mínimo um número.")
                .Matches("[^a-zA-Z0-9]").WithMessage("A senha deve conter no mínimo um caractere especial.");

            return options;
        }
    }
}
