﻿namespace SejaFlor.Domain.Enumerations
{
    public enum PaymentTypeEnum
    {
        Cash = 1,
        CreditCard = 2,
        DebitCard = 3,
        Financing = 4
    }
}
