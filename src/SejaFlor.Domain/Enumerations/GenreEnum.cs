﻿namespace SejaFlor.Domain.Enumerations
{
    public enum GenreEnum
    {
        Feminino = 1,
        Masculino = 2
    }
}
