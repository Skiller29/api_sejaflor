﻿using System;

namespace SejaFlor.Domain.Entities
{
    public class Audit
    {
        public DateTime CreationTime { get; private set; }
        public Guid CreationUser { get; private set; }
        public DateTime LasModificationTime { get; private set; }
        public Guid ModifierUser { get; private set; }
        public DateTime DeletionTime { get; private set; }
        public Guid DeletionUser { get; private set; }
        public bool IsDeleted { get; private set; }
    }
}
