﻿using FluentValidation;
using SejaFlor.Domain.Extensions;
using System;
using System.Collections.Generic;

namespace SejaFlor.Domain.Entities
{
    public class User : Entity<User>
    {
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime CreationTime { get; private set; }
        public bool IsDeleted { get; private set; }
        public DateTime? DeletionTime { get; private set; }

        public virtual ICollection<Sale> Installments { get; private set; }

        public User(
            string userName,
            string password,
            string firstName,
            string lastName)
        {
            UserName = userName;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
        }

        private User() { }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateUserName();
            ValidatePassword();
            ValidateEmail();
            ValidateFirstName();
            ValidateLastName();
        }

        private void ValidateUserName()
        {
            RuleFor(c => c.UserName)
                .NotEmpty().WithMessage("Por favor, informe o username do usuário.")
                .Length(3, 50).WithMessage("O username do usuário deve ter entre 3 e 50 caracteres.");
        }

        private void ValidatePassword()
        {
            RuleFor(c => c.Password)
                .NotEmpty().WithMessage("Por favor, informe a senha do usuário.")
                .Length(6, 12).WithMessage("A senha do usuário deve ter entre 6 e 12 caracteres.")
                .IsPassword();                        
        }

        private void ValidateEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Por favor, informe o e-mail do usuário.")
                .Length(3, 50).WithMessage("O e-mail do usuário deve ter entre 3 e 70 caracteres.")
                .EmailAddress().WithMessage("Por favor, informe um e-mail válido.");
        }

        private void ValidateFirstName()
        {
            RuleFor(c => c.FirstName)
                .NotEmpty().WithMessage("Por favor, informe o primeiro nome do usuário.")
                .Length(3, 30).WithMessage("O primeiro nome do usuário deve ter entre 3 e 30 caracteres");
        }

        private void ValidateLastName()
        {
            RuleFor(c => c.LastName)
                .NotEmpty().WithMessage("Por favor, informe o último nome do usuário.")
                .Length(3, 80).WithMessage("O último nome do usuário deve ter entre 3 e 80 caracteres");
        }
        #endregion        
    }
}
