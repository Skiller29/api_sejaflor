﻿using FluentValidation;
using SejaFlor.Domain.Enumerations;
using System;
using System.Collections.Generic;

namespace SejaFlor.Domain.Entities
{
    public class Customer :  Entity<Customer>
    {       
        public string Name { get; private set; }
        public string Phone { get; private set; }
        public string Address { get; private set; }
        public string Neighborhood { get; private set; }
        public int Genre { get; private set; }
        public string Reference { get; private set; }        
        public Audit Audit { get; private set; }

        public virtual ICollection<Sale> SalesList { get; private set; }

        public Customer(
            string name,
            string phone,
            string address,
            string neighborhood,
            GenreEnum genreEnum,
            string reference)
        {
            Name = name;
            Phone = phone;
            Address = address;
            Neighborhood = neighborhood;
            Genre = (int)genreEnum;
            Reference = reference;
        }

        private Customer() { }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateName();
            ValidatePhone();
            ValidateAddress();
            ValidateNeighborhood();
            ValidateGenre();
        }

        private void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Por favor, informe o nome do cliente.")
                .Length(2, 100).WithMessage("O nome do cliente deve ter entre 2 e 100 caracteres");
        }

        private void ValidatePhone()
        {
            RuleFor(c => c.Phone)                
                .Length(8, 11).WithMessage("O telefone deve ter entre 8 e 11 caracteres.");
        }

        private void ValidateAddress()
        {
            RuleFor(c => c.Address)
                .Length(2, 150).WithMessage("O endereço deve ter entre 2 e 150 caracteres.");
        }

        private void ValidateNeighborhood()
        {
            RuleFor(c => c.Neighborhood)               
                .Length(2, 60).WithMessage("O endereço deve ter entre 2 e 150 caracteres.");
        }

        private void ValidateGenre()
        {
            RuleFor(c => c.Genre)
                .IsInEnum().WithMessage("Gênero não encontrado.");
        }
        #endregion
    }
}
