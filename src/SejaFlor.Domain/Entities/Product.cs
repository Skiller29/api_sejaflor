﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SejaFlor.Domain.Entities
{
    public class Product : Entity<Product>
    {
        public string Name { get; private set; }
        public decimal CostAmount { get; private set; }
        public decimal SaleAmountInCash { get; private set; }
        public decimal SaleAmountInCard { get; private set; }
        public string MainPictureUrl { get; private set; }
        public Audit Audit { get; private set; }

        public virtual ICollection<SaleItem> SaleItemsList { get; private set; }

        public Product(
            string name,
            decimal costAmount,
            decimal saleAmountInCash,
            decimal saleAmountInCard,
            string mainPictureUrl)
        {
            Name = name;
            CostAmount = costAmount;
            SaleAmountInCash = saleAmountInCash;
            SaleAmountInCard = saleAmountInCard;
            MainPictureUrl = mainPictureUrl;
        }

        private Product() { }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateName();
            ValidateCostAmount();
            ValidateSaleAmountInCash();
            ValidateSaleAmountInCard();
        }

        private void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Por favor, informe o nome do produto.")
                .Length(2, 100).WithMessage("O nome do produto deve ter entre 2 e 100 caracteres");
        }

        private void ValidateCostAmount()
        {
            RuleFor(c => c.CostAmount)                
                .ExclusiveBetween(0, 10000000).WithMessage("O valor de custo deve ser entre 0 e 10.000.000");
        }

        private void ValidateSaleAmountInCash()
        {
            RuleFor(c => c.SaleAmountInCash)
                .ExclusiveBetween(0, 10000000).WithMessage("O valor de venda em dinheiro deve ser entre 0 e 10.000.000");
        }

        private void ValidateSaleAmountInCard()
        {
            RuleFor(c => c.SaleAmountInCard)
                .ExclusiveBetween(0, 10000000).WithMessage("O valor de venda em cartão deve ser entre 0 e 10.000.000");
        }
        #endregion
    }
}
