﻿using FluentValidation;
using System;

namespace SejaFlor.Domain.Entities
{
    public class SaleItem : Entity<SaleItem>
    {
        public int Quantity { get; private set; }
        public decimal Amount { get; private set; }
        public Guid ProductId { get; private set; }
        public Guid SaleId { get; private set; }

        public virtual Product Product { get; private set; }
        public virtual Sale Sale { get; private set; }

        public SaleItem(
            int quantity,
            decimal amount,
            Guid productId,
            Guid saleId)
        {
            Quantity = quantity;
            Amount = amount;
            ProductId = productId;
            SaleId = saleId;
        }

        private SaleItem() { }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateQuantity();
            ValidateAmount();
            ValidateProductId();
            ValidateSaleId();
        }

        private void ValidateQuantity()
        {
            RuleFor(c => c.Quantity)
                    .GreaterThan(0).WithMessage("A quantidade deve ser maior que 0.");
        }

        private void ValidateAmount()
        {
            RuleFor(c => c.Amount)
                    .GreaterThan(0).WithMessage("O Valor do item deve ser maior que 0.");
        }

        private void ValidateProductId()
        {
            RuleFor(c => c.ProductId)
                   .NotEqual(Guid.Empty).WithMessage("Por favor, vincule um produto a venda.");
        }

        private void ValidateSaleId()
        {
            RuleFor(c => c.SaleId)
                   .NotEqual(Guid.Empty).WithMessage("Por favor, vincule o id da venda.");
        }
        #endregion        
    }
}
