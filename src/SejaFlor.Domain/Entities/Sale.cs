﻿using FluentValidation;
using SejaFlor.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace SejaFlor.Domain.Entities
{
    public class Sale : Entity<Sale>
    {
        public DateTime SaleDate { get; private set; }
        public decimal Total { get; private set; }
        public decimal TotalCost { get; private set; }
        public bool Installment { get; private set; }
        public int? NumberInstallments { get; private set; }
        public DateTime? DueDate { get; private set; }
        public int PaymentTypeId { get; private set; }
        public Guid CustomerId { get; private set; }

        public virtual PaymentType PaymentType { get; private set; }
        public virtual Customer Customer { get; private set; }

        public virtual ICollection<SaleItem> SaleItemsList { get; private set; }
        public virtual ICollection<Installment> InstallmentsList { get; private set; }

        public Sale(
            DateTime saleDate,
            decimal total, 
            decimal totalCost,
            bool installment,
            int? numberInstallments,
            DateTime? dueDate,
            PaymentTypeEnum paymentTypeEnum,
            Guid customerId)
        {
            SaleDate = SaleDate;
            Total = total;
            TotalCost = totalCost;
            Installment = installment;
            PaymentTypeId = (int)paymentTypeEnum;
            CustomerId = customerId;

            if (installment)
            {
                NumberInstallments = numberInstallments;
                DueDate = dueDate;
            }                                                
        }

        private Sale() { }

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateSaleDate();
            ValidateTotal();
            ValidateTotalCost();
            ValidateNumberInstallments();
            ValidateDueDate();
            ValidatePaymentTypeId();
            ValidateCustomerId();
        }

        private void ValidateSaleDate()
        {
            RuleFor(c => c.SaleDate)
                    .GreaterThan(DateTime.MinValue).WithMessage("Não é possível utilizar esta data.")
                    .LessThan(DateTime.MaxValue).WithMessage("Não é possível utilizar esta data.");
        }

        private void ValidateTotal()
        {
            RuleFor(c => c.Total)
                    .GreaterThan(0).WithMessage("O total deve ser maior que 0.");
        }

        private void ValidateTotalCost()
        {
            RuleFor(c => c.NumberInstallments)
                    .GreaterThan(0).WithMessage("O total de custo deve ser maior que 0.");
        }

        private void ValidateNumberInstallments()
        {
            RuleFor(c => c.NumberInstallments)
                    .GreaterThan(0).WithMessage("O número de parcelas não pode ser menor que 1.");
        }

        private void ValidateDueDate()
        {
            RuleFor(c => c.DueDate)
                    .GreaterThan(DateTime.MinValue).WithMessage("Não é possível utilizar esta data.")
                    .LessThan(DateTime.MaxValue).WithMessage("Não é possível utilizar esta data.");
        }

        private void ValidatePaymentTypeId()
        {
            //RuleFor(c => c.)
            //    .IsInEnum<PaymentTypeEnum, PaymentType>().WithMessage("Gênero não encontrado.");
        }

        private void ValidateCustomerId()
        {
            RuleFor(c => c.CustomerId)
                   .NotEqual(Guid.Empty).WithMessage("Por favor, vincule um cliente a venda.");
        }
        #endregion
    }
}
