﻿using System;
using FluentValidation;

namespace SejaFlor.Domain.Entities
{
    public class Installment : Entity<Installment>
    {       
        public DateTime DueDate { get; private set; }
        public decimal Amount { get; private set; }
        public int NumberInstallment { get; private set; }
        public bool IsPaid { get; private set; }
        public DateTime? PaymentDate { get; private set; }
        public Guid SaleId { get; private set; }
        public Guid? PayerUserId { get; private set; }

        public virtual Sale Sale { get; private set; }
        public virtual User PayerUser { get; private set; }

        public Installment(
            DateTime dueDate,
            decimal amount,
            int numberInstallment,
            bool isPaid,
            DateTime? paymentDate,
            Guid saleId,
            Guid? payerUserId)
        {
            DueDate = dueDate;
            Amount = amount;
            NumberInstallment = numberInstallment;
            IsPaid = isPaid;            
            SaleId = saleId;
            
            if(IsPaid)
            {
                PayerUserId = payerUserId;
                PaymentDate = paymentDate;
            }
        }

        private Installment() { }

        public void PayInstallment(Guid payerUserId)
        {
            PaymentDate = DateTime.Now;
            IsPaid = true;
            PayerUserId = payerUserId;
        }

        #region Methods
        private void PayInstallment(decimal amount)
        {
            PaymentDate = DateTime.Now;
            IsPaid = true;
            Amount = amount;
        }
        #endregion

        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        #region Validations
        private void Validate()
        {
            ValidateDueDate();
            ValidateAmount();
            ValidateNumberInstallment();
            ValidatePaymentDate();
            ValidateSaleId();
            ValidatePayerUserId();
        }

        private void ValidateDueDate()
        {
            RuleFor(c => c.DueDate)
                    .GreaterThan(DateTime.MinValue).WithMessage("Não é possível utilizar esta data.")
                    .LessThan(DateTime.MaxValue).WithMessage("Não é possível utilizar esta data.");
        }

        private void ValidateAmount()
        {
            RuleFor(c => c.Amount)
                    .ExclusiveBetween(0, 10000000).WithMessage("O valor da parcela deve ser entre 0 e 10.000.000");
        }

        private void ValidateNumberInstallment()
        {
            RuleFor(c => c.NumberInstallment)
                    .GreaterThan(0).WithMessage("O número da parcela não pode ser menor que 1.");
        }

        private void ValidatePaymentDate()
        {
            RuleFor(c => c.PaymentDate)
                    .GreaterThan(DateTime.MinValue).WithMessage("Não é possível utilizar esta data.")
                    .LessThan(DateTime.MaxValue).WithMessage("Não é possível utilizar esta data.");
        }

        private void ValidateSaleId()
        {
            RuleFor(c => c.SaleId)
                   .NotEqual(Guid.Empty).WithMessage("Por favor, vincule uma venda a parcela.");
        }

        private void ValidatePayerUserId()
        {
            RuleFor(c => c.PayerUserId)
                   .NotEqual(Guid.Empty).WithMessage("Por favor, vincule um usuário a parcela.");
        }
        #endregion


    }
}
