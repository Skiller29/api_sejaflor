﻿
namespace SejaFlor.Domain.Entities
{
    public class PaymentType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
