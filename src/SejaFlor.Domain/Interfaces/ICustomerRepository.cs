﻿using SejaFlor.Domain.Entities;
using SejaFlor.Domain.Interfaces.Base;

namespace SejaFlor.Domain.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {

    }
}
