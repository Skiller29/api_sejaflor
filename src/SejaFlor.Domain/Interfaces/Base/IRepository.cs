﻿using SejaFlor.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SejaFlor.Domain.Interfaces.Base
{
    public interface IRepository<TEntity> : IDisposable where TEntity : Entity<TEntity>
    {
        void Insert(TEntity obj);
        void Update(TEntity obj);
        IEnumerable<TEntity> List(Expression<Func<TEntity, bool>> predicate);
        TEntity GetById(Guid id);
        IEnumerable<TEntity> GetAll();
        void Delete(Guid id);
    }
}
