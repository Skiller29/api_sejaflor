﻿using Microsoft.EntityFrameworkCore;
using SejaFlor.Domain.Entities;
using SejaFlor.Domain.Interfaces.Base;
using SejaFlor.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SejaFlor.Infra.Data.Repository.Base
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity<TEntity>
    {
        protected readonly SejaFlorContext _context;
        private readonly DbSet<TEntity> _dbSet; 

        protected Repository(SejaFlorContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public virtual void Insert(TEntity obj)
        {
            _dbSet.Add(obj);
        }

        public virtual void Update(TEntity obj)
        {
            _dbSet.Update(obj);
        }

        public virtual IEnumerable<TEntity> List(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate);
        }

        public virtual TEntity GetById(Guid id)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(t => t.Id == id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual void Delete(Guid id)
        {
            _dbSet.Remove(_dbSet.Find(id));
        }
    }
}
