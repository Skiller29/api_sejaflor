﻿using SejaFlor.Domain.Entities;
using SejaFlor.Domain.Interfaces;
using SejaFlor.Infra.Data.Context;
using SejaFlor.Infra.Data.Repository.Base;

namespace SejaFlor.Infra.Data.Repository
{
    public class SaleRepository : Repository<Sale>, ISaleRepository
    {
        public SaleRepository(SejaFlorContext context) : base(context)
        {
        }
    }
}
