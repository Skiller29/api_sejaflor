﻿using SejaFlor.Domain.Entities;
using SejaFlor.Domain.Interfaces;
using SejaFlor.Infra.Data.Context;
using SejaFlor.Infra.Data.Repository.Base;

namespace SejaFlor.Infra.Data.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(SejaFlorContext context) 
            : base(context)
        {
        }
    }
}
