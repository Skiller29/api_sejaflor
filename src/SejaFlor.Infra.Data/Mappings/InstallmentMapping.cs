﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SejaFlor.Domain.Entities;
using System;

namespace SejaFlor.Infra.Data.Mappings
{
    public class InstallmentMapping : IEntityTypeConfiguration<Installment>
    {
        public void Configure(EntityTypeBuilder<Installment> builder)
        {
            builder.ToTable("Installment");

            builder.HasAnnotation("Relational:TableName", "Installment");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "InstallmentId");

            builder.HasOne(i => i.Sale)
           .WithMany(s => s.InstallmentsList)
           .HasForeignKey(s => s.SaleId)
           .HasConstraintName("FK_Installment_Sale");
        }
    }
}
