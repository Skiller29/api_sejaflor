﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SejaFlor.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SejaFlor.Infra.Data.Mappings
{
    public class SaleItemMapping : IEntityTypeConfiguration<SaleItem>
    {
        public void Configure(EntityTypeBuilder<SaleItem> builder)
        {
            builder.ToTable("SaleItem");

            builder.HasAnnotation("Relational:TableName", "SaleItem");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "SaleItemId");

            builder.HasOne(s => s.Product)
            .WithMany(p => p.SaleItemsList)
            .HasForeignKey(s => s.ProductId)
            .HasConstraintName("FK_SaleItem_Product");

            builder.HasOne(si => si.Sale)
            .WithMany(s => s.SaleItemsList)
            .HasForeignKey(s => s.SaleId)
            .HasConstraintName("FK_SaleItem_Sale");
        }
    }
}
