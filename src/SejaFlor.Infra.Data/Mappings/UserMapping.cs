﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SejaFlor.Domain.Entities;

namespace SejaFlor.Infra.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.HasAnnotation("Relational:TableName", "User");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "UserId");
        }
    }
}
