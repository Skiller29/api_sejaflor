﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SejaFlor.Domain.Entities;
using System;

namespace SejaFlor.Infra.Data.Mappings
{
    public class SaleMapping : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.ToTable("Sale");

            builder.HasAnnotation("Relational:TableName", "Sale");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "SaleId");

            builder.HasOne(s => s.Customer)
              .WithMany(c => c.SalesList)
              .HasForeignKey(s => s.CustomerId)
              .HasConstraintName("FK_Sale_Customer");            
        }
    }
}
