﻿using Microsoft.EntityFrameworkCore;
using SejaFlor.Domain.Entities;
using SejaFlor.Infra.Data.Mappings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SejaFlor.Infra.Data.Context
{
    public class SejaFlorContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleItem> SaleItems { get; set; }
        public DbSet<Sale> Installments { get; set; }
      
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMapping());
            modelBuilder.ApplyConfiguration(new CustomerMapping());
            modelBuilder.ApplyConfiguration(new ProductMapping());
            modelBuilder.ApplyConfiguration(new SaleMapping());
            modelBuilder.ApplyConfiguration(new SaleItemMapping());
            modelBuilder.ApplyConfiguration(new InstallmentMapping());


            modelBuilder.Entity<Customer>().HasQueryFilter(c => !c.Audit.IsDeleted);
            modelBuilder.Entity<Product>().HasQueryFilter(p => !p.Audit.IsDeleted);

            base.OnModelCreating(modelBuilder);
        }
    }
}
